Example 9: Constructing an Arithmetic Expression Tree
========


This example demonstrates how to convert a provided arithmetic expression into an arithmetic expression tree. The example only works with fully parenthesized expressions with only three operations: addition, subtraction and multiplication.

The purpose of this example is to demonstrate the following:
+ how to create simple _subtypes_ using _subclassing_ in Java (`extends`);
+ how to parse a simple arithmetic expression;
+ how to construct a binary tree;
+ how to exploit the recursive nature of a binary tree;
+ how to convert an arithmetic expression into its Postfix form.

### Suggested Reading and Viewing
+ Tutorial on [subclasses](http://docs.oracle.com/javase/tutorial/java/IandI/subclasses.html) in Java
+ [Binary trees](https://dl.dropboxusercontent.com/u/567187/EECE%20210/Java/BinaryTrees.pdf)
+ Videos on binary trees: [Video 1](http://media.pearsoncmg.com/aw/aw_reges_bjp_2/videoPlayer.php?id=c17-1), [Video 2](http://media.pearsoncmg.com/aw/aw_reges_bjp_2/videoPlayer.php?id=c17-2), [Video 3](http://www.youtube.com/watch?v=FKvL3Duawv8)
+ [Postfix notation](http://en.wikipedia.org/wiki/Reverse_Polish_notation)

### Examining the Source Code

#### Creating a binary tree

**A recursive datatype**

A binary tree is a recursive datatype. Such datatypes are built using the fact that most modern object-oriented programming languages allow an object to include references to other objects  of the _same type_. This gives rise to the _recursive_ nature of the datatype because the datatype is defined in terms of other references to the same datatype.

In this example, the `Node` class is defined as follows:

```java
public abstract class Node {

	// How many children does this node have? 
	// We are mostly dealing with either two children or none.
	private int numChildren;
	
	// References to the child nodes.
	Node[] childNodes;
	…
}
```

A `Node` contains an array of references to other `Node` objects.

To create a `Node` with two children, the constructor is as follows:

```java
	/**
	 * A constructor that takes two child nodes and creates a node.
	 * @param leftChild
	 * @param rightChild
	 */
	public Node(Node leftChild, Node rightChild) {
		this.numChildren = 2;
		childNodes = new Node[2];
		childNodes[0] = leftChild;
		childNodes[1] = rightChild;
	}
```

When a every node has at most two children, and the children are not references to other `Node` objects that are higher up (earlier generation `Node`s) in the same structure, we get a _binary tree_.

**Subtypes, subclasses and inheritance**

The `abstract` `Node` class is `extend`ed by other classes that represent specific parts of an arithmetic expression. Note that it is not possible to create objects of an `abstract` class but one can create objects of a concrete subclass and refer to them using the parent class. An example of `extend`ing a class is the `PlusNode` class:

```java
public class PlusNode extends Node {

	/**
	 * Create a new node that represents an addition operation. The two children
	 * of this node represent the two operands, although they may be
	 * subexpressions that need to be evaluated.
	 * 
	 * @param leftNode
	 * @param rightNode
	 */
	public PlusNode(Node leftNode, Node rightNode) {
		super(leftNode, rightNode);
	}

	/**
	 * Return a String that represents the operation this node represents.
	 * 
	 * @return "+" for addition.
	 */
	public String getOpName() {
		return "+";
	}
}
```

**From expression to tree**

A binary tree representing an arithmetic expression is created given a `String` that represents the arithmetic expression. The code that achieves this is in the following method that is part of `ArithmeticExpressionTree.java`.

```java
	/**
	 * Recursive helper method of the public constructor. Reads an expression
	 * specification from the scanner until the parentheses (if any) are matched
	 * and creates the corresponding tree. Note: <code>createTree</code> does
	 * not read until the end of the input, only to the end of the current
	 * subexpression.
	 * 
	 * @param scanner
	 *            the token stream representing the part of the expression that
	 *            has not been read yet.
	 * @throws MalformedExpressionException
	 *             if the input expression is not valid.
	 */
	private Node createTree(Scanner scanner)
			throws MalformedExpressionException {
		// Read the next token from the scanner.
		Token nextTok = scanner.getToken();
		// If it's null, then the expression must be wrong.
		if (nextTok == null) {
			throw new MalformedExpressionException(
					"Input expression ended prematurely");
		}

		// Otherwise, check if it's the beginning of a subexpression.
		if (nextTok.equals("(")) {
			// It's a subexpression; eat the "(" token.
			scanner.eatToken();

			// Read the operator and the operands and create left and right
			// subtrees for the operands.
			Node leftExpr = createTree(scanner);
			Token opTok = scanner.getToken();

			// If opTok is null, then the expression is wrong.
			if (opTok == null) {
				throw new MalformedExpressionException(
						"Input expression ended prematurely");
			}

			String op = opTok.getName();
			// Eat the operator token.
			scanner.eatToken();
			Node rightExpr = createTree(scanner);

			// Make sure the next token is ")"; otherwise, the input is
			// bad.
			scanner.useToken(")");

			// Create the root of a subtree to represent the expression
			// just read.
			switch (op) {
			case "+":
				return new PlusNode(leftExpr, rightExpr);
			case "-":
				return new SubtractNode(leftExpr, rightExpr);
			case "*":
				return new MultiplyNode(leftExpr, rightExpr);
			default:
				throw new MalformedExpressionException();
			}

		} else {
			// The expression is just a single value; create a leaf node
			// for it.
			BigDecimal value;
			if (nextTok.isInteger()) {
				value = nextTok.getValue();
			} else {
				throw new MalformedExpressionException();
			}

			// Eat the integer/variable token.
			scanner.eatToken();
			return new OperandNode(value);
		}
	}
```

#### Obtaining a Postfix representation of an arithmetic expression

Once an arithmetic expression has been represented as a binary tree, it is easy to create a Postfix representation of the original expression. This is achieved through what is known as a _post order traversal_ of the binary tree. In a post order traversal, the child nodes of a node are processed first and then the node itself is processed.

Exploiting the recursive nature of a tree, the code is simply the following (in `ArithmeticExpressionTree.java`):

```java
public String getPostfixExpr() {
		return getPostfixExpr(expression);
	}

	private String getPostfixExpr(Node root) {
		// If the node is a leaf, just print the value.
		if (root.isLeaf()) {
			return root.getOpName();
		} else {
			return getPostfixExpr(root.getChild(0)) + " "
					+ getPostfixExpr(root.getChild(1)) + " "
					+ root.getOpName();
		}
	}
```

#### Test code

`JUnit` is used to test the implementation. Create your own tests and see if the implementation is robust.

```java
import static org.junit.Assert.*;

import org.junit.Test;
import java.math.BigDecimal;

public class ArithmeticExpressionTreeTest {
	@Test
	public void test1() {
		String expr = "((1+2)+3)";
		ArithmeticExpressionTree myTree;
		try {
			myTree = new ArithmeticExpressionTree(expr);
			assertEquals(myTree.getPostfixExpr(), "1 2 + 3 +");
		}
		catch( MalformedExpressionException e ) {
			fail("Exception!");
		}
		
	}

	@Test
	public void test2() {
		String expr = "((1+2)*3)";
		ArithmeticExpressionTree myTree;
		try {
			myTree = new ArithmeticExpressionTree(expr);
			System.out.println(myTree.getPostfixExpr());
			assertEquals(myTree.getPostfixExpr(), "1 2 + 3 *");		}
		catch( MalformedExpressionException e ) {
			fail("Exception!");
		}
		
	}
	
	@Test
	public void test3() {
		String expr = "((1+2)/3)";
		ArithmeticExpressionTree myTree;
		try {
			myTree = new ArithmeticExpressionTree(expr);
			fail("Should have thrown an exception because we have not implemented divide yet!");
		}
		catch( Exception e ) {
			assertEquals(1,1);
		}
		
	}
	
}
```