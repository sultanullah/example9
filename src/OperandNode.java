import java.math.BigDecimal;

/**
 * An operand node extends Node. This node represents a number and is a leaf
 * node.
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public class OperandNode extends Node {

	static final int numChildren = 0;

	// The value at the node
	private BigDecimal value;

	/**
	 * A constructor that creates an Operand Node by taking a value.
	 * 
	 * @param val
	 *            the value at the node.
	 */
	public OperandNode(BigDecimal val) {
		super();
		value = val;
	}

	/**
	 * For an Operand Node, this method returns the value/number at this node.
	 * 
	 * @return a String representation of the number at the node
	 */
	public String getOpName() {
		return value.toString();
	}

}
