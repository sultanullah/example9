import java.math.BigDecimal;

public class SubtractNode extends Node {

	static final int numChildren = 2;

	/**
	 * Create a new node that represents a subtraction operation. The two
	 * children of this node represent the two operands, although they may be
	 * subexpressions that need to be evaluated.
	 * 
	 * @param leftNode
	 * @param rightNode
	 */
	public SubtractNode(Node leftNode, Node rightNode) {
		super(leftNode, rightNode);
	}

	/**
	 * Return a String that represents the operation this node represents.
	 * 
	 * @return "-" for addition.
	 */
	public String getOpName() {
		return "-";
	}

}
