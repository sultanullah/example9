/**
 * Exception thrown when an invalid arithmetic expression is encountered by the
 * program. The message field contains information about the particular problem
 * that was encountered.
 * 
 * @see #getMessage()
 */

public class MalformedExpressionException extends Exception {

	/**
	 * Constructs a MalformedExpressionException with no message.
	 */
	public MalformedExpressionException() {
		super();
	}

	/**
	 * Constructs a MalformedExpressionException with the detail message.
	 * @param the message that accompanies the exception
	 */
	public MalformedExpressionException(String message) {
		super(message);
	}
}