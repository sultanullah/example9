import java.math.BigDecimal;

public class PlusNode extends Node {

	/**
	 * Create a new node that represents an addition operation. The two children
	 * of this node represent the two operands, although they may be
	 * subexpressions that need to be evaluated.
	 * 
	 * @param leftNode
	 * @param rightNode
	 */
	public PlusNode(Node leftNode, Node rightNode) {
		super(leftNode, rightNode);
	}

	/**
	 * Return a String that represents the operation this node represents.
	 * 
	 * @return "+" for addition.
	 */
	public String getOpName() {
		return "+";
	}
}
