import java.math.BigDecimal;

public class MultiplyNode extends Node {

	static final int numChildren = 2;

	/**
	 * Create a new node that represents a multiplication operation. The two
	 * children of this node represent the two operands, although they may be
	 * subexpressions that need to be evaluated.
	 * 
	 * @param leftNode
	 * @param rightNode
	 */
	public MultiplyNode(Node leftNode, Node rightNode) {
		super(leftNode, rightNode);
	}


	/**
	 * Return a String that represents the operation this node represents.
	 * 
	 * @return "*" for multiplication.
	 */
	public String getOpName() {
		return "*";
	}

}
